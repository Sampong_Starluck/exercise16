import Router from "vue-router";
import Vue from "vue";

import customerList from './components/customerList.vue';
import addCustomer from './components/addCustomer.vue';
import searchCustomer from './components/searchCustomer.vue';
import customer from './components/customer.vue';

Vue.use(Router);
export default new Router({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "customer",
            alias: "/customer",
            component: customerList,
            children: [{
                path: "/customer/:id",
                name: "customer-infos",
                component: customer,
                props: true
            }]
        },
        {
            path: "/add",
            name: "add",
            component: addCustomer
        },
        {
            path: "/search",
            name: "search",
            component: searchCustomer
        }
    ]
});