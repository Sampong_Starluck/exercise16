package itc.gic.backend.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import itc.gic.backend.models.customer;
import itc.gic.backend.repo.repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@CrossOrigin(origins = "http://localhst:8080")
@RestController
@RequestMapping("/api")
public class controller {
    
    @Autowired
    repository repo;

    @GetMapping("/customer")
    public List getAllInfo() {
        System.out.println("Get All Customers Infor....");

        List cutomers = new ArrayList<>();
        repo.findAll().forEach(cutomers::add);

        return cutomers;
    }
    @PostMapping(value="/customer")
    public customer postCustomer(@RequestBody customer customer) {
        customer customers = (itc.gic.backend.models.customer) repo
                .save(new customer(customer.getName(), customer.getTelephone(), customer.getEnterDate(),
                        customer.getExitDate(), customer.getAge(), customer.getId()));

        return customers;
    }
    
    @DeleteMapping("/customer/{id}")
    public ResponseEntity deleteCutomer(@PathVariable("id") int id) {
        System.out.println("Deleting Customer with ID: " + id + "....");

        repo.deleteById(id);

        return new ResponseEntity<>("Customer has been delete", HttpStatus.OK);
    }
    @GetMapping("/customer/age/{age}")
    public List findByAge(@PathVariable int age) {
        List customer = repo.findByAge(age);
        return customer;
    }
    
    @PutMapping("/customer/{id}")
    public ResponseEntity updatecustomer(@PathVariable("id") int id, @RequestBody customer customers) {
        System.out.println("Update Customer With ID: " + id + ".....");

        Optional data = repo.findById(id);

        if (data.isPresent()) {
            customer _customer = (customer) data.get();
            _customer.setName(customers.getName());
            _customer.setAge(customers.getAge());
            _customer.setTelephone(customers.getTelephone());
            _customer.setEnterDate(customers.getEnterDate());
            _customer.setExitDate(customers.getExitDate());
            return new ResponseEntity<>(repo.save(_customer), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
