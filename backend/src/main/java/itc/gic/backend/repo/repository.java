package itc.gic.backend.repo;

import java.util.*;

import org.springframework.data.repository.CrudRepository;
import itc.gic.backend.models.customer;

public interface repository extends CrudRepository {
    List findByAge(int age);
}
