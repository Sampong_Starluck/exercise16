package itc.gic.backend;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class homePage {
    @GetMapping("/")
    public String home() {
        return "Hi I'm Sampong";
    }
}
