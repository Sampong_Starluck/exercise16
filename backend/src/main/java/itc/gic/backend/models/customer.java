package itc.gic.backend.models;

import javax.persistence.*;

@Entity
@Table(name="people")
public class customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name="name")
    private String name;

    @Column(name = "age")
    private int age;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "enterDate")
    private String enterDate;

    @Column(name = "exitDate")
    private String exitDate;


    public customer(String name, String telephone, String enterDate, String exitDate, int id, int age) {
        this.name = name;
        this.age   = age;
        this.telephone = telephone;
        this.id = id;
        this.enterDate = enterDate;
        this.exitDate   = exitDate;
        
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEnterDate() {
        return this.enterDate;
    }

    public void setEnterDate(String enterDate) {
        this.enterDate = enterDate;
    }

    public String getExitDate() {
        return this.exitDate;
    }

    public void setExitDate(String exitDate) {
        this.exitDate = exitDate;
    }

    @Override
    public String toString() {
        return "Customer [id=" + id + ", name=" + name + ", age=" + age + ", telephone= " + telephone + "enter date="+enterDate +"exit date= "+exitDate +"]";
    }
}
